//
//  ViewController.swift
//  Project6b
//
//  Created by Vrushali Kulkarni on 22/05/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let label1 = UILabel()
    let label2 = UILabel()
    let label3 = UILabel()
    let label4 = UILabel()
    let label5 = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareLabels()
        addConstraintsToLabels()
    }

    func prepareLabels() {

        label1.translatesAutoresizingMaskIntoConstraints = false
        label1.text = "THESE"
        label1.textAlignment = .center
        label1.backgroundColor = .red
        label1.sizeToFit()

        label2.translatesAutoresizingMaskIntoConstraints = false
        label2.text = "ARE"
        label2.textAlignment = .center
        label2.backgroundColor = .cyan
        label2.sizeToFit()

        label3.translatesAutoresizingMaskIntoConstraints = false
        label3.text = "SOME"
        label3.textAlignment = .center
        label3.backgroundColor = .green
        label3.sizeToFit()

        label4.translatesAutoresizingMaskIntoConstraints = false
        label4.text = "AWESOME"
        label4.textAlignment = .center
        label4.backgroundColor = .blue
        label4.sizeToFit()

        label5.translatesAutoresizingMaskIntoConstraints = false
        label5.text = "LABELS"
        label5.textAlignment = .center
        label5.backgroundColor = .yellow
        label5.sizeToFit()

        view.addSubview(label1)
        view.addSubview(label2)
        view.addSubview(label3)
        view.addSubview(label4)
        view.addSubview(label5)
    }

    func addConstraintsToLabels() {
// OPTION 1
//        let viewsDictionary = ["label1": label1,
//                               "label2": label2,
//                               "label3": label3,
//                               "label4": label4,
//                               "label5": label5]
//
//        for label in viewsDictionary.keys {
//
//            let constraint = NSLayoutConstraint.constraints(withVisualFormat: "H:|[\(label)]|",
//                                                            options: [],
//                                                            metrics: nil,
//                                                            views: viewsDictionary)
//            view.addConstraints(constraint)
//        }
//
//        let constraint = """
//        V:|
//        [label1(labelHeight@999)]-
//        [label2(label1)]-
//        [label3(label1)]-
//        [label4(label1)]-
//        [label5(label1)]->=10-|
//        """
//
//        let metrics = ["labelHeight": 88]
//        let verticalConstraint = NSLayoutConstraint.constraints(withVisualFormat: constraint,
//                                                               options: [],
//                                                               metrics: metrics,
//                                                               views: viewsDictionary)
//        view.addConstraints(verticalConstraint)
// OPTION 2
        var previous: UILabel?
        let labels = [label1, label2, label3, label4, label5]
        for label in labels {
//            label.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
            if label == labels.last {
                label.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.2, constant: 0).isActive = true
            } else {
                label.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.2, constant: -10).isActive = true
            }
            label.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
            label.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true

            if let previous = previous {
                label.topAnchor.constraint(equalTo: previous.safeAreaLayoutGuide.bottomAnchor, constant: 10)
                    .isActive = true
            } else {
                label.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0)
                    .isActive = true
            }
            previous = label
        }
    }
}
